class User < ApplicationRecord
	# validates_uniquess_of :email
	# validates :email, uniqueness: true, precense: true
	validates_uniqueness_of :email
	validates_presence_of :first_name, :last_name
end
