Rails.application.routes.draw do
  get 'home/index'

  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  get "users/sign_up"
  get "users/sign_in"
  resources :users
  root "home#index"
end
